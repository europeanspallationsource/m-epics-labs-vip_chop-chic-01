# m-epics-labs-vip_chop-chic-01

This module was created by PLCFactory by running the following command:

`python ../ics_plc_factory/plcfactory.py --plc-beckhoff --eee -d LabS-VIP:Chop-CHIC-01`

It is intended to be an EPICS interface to the Chopper Integrated Controller
and mini-chopper of the Vertical Integration Project in the ICS Utgard lab.
